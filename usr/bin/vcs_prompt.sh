#!/bin/bash

function in_termux {
	command -v termux-reload-settings > /dev/null 2>&1 && return

	return 1
}

function in_git_repo {
	#Check to see if git is installed
	command -v git > /dev/null 2>&1 || return

	#Use branch as the test for if in a repo
	git branch > /dev/null 2>&1 && return
	return 1
}

function in_mercurial_repo {
	#Check to see if hg is installed
	command -v hg > /dev/null 2>&1 || return

	#Use root as the test for if in a repo
	hg root > /dev/null 2>&1 && return
	return 1
}

function in_svn_repo {
	#Check to see if svn is installed
	command -v svn > /dev/null 2>&1 || return

	#Use root as the test for if in a repo
	svn info > /dev/null 2>&1 && return
	return 1
}

function in_repo {
	(in_git_repo || in_mercurial_repo || in_svn_repo) && return
	return 1
}

function vcs_prompt {
  if in_git_repo; then
    GIT_PROMPT_SCRIPT=/usr/lib/git-core/git-sh-prompt
    if in_termux; then
      GIT_PROMPT_SCRIPT=/data/data/com.termux/files/usr/etc/bash_completion.d/git-prompt.sh
    fi
    . $GIT_PROMPT_SCRIPT

    GIT_PS1_SHOWDIRTYSTATE=true
    GIT_PS1_SHOWCOLORHINTS=true
    GIT_PS1_UNTRACKEDFILES=true

    __git_ps1 "${DEFAULT_PS1} git" '\n\\$ '
  elif in_mercurial_repo; then
      local s=$(hg prompt "${NO_COLOR}(${GREEN}{branch} ${RED}{status}${NO_COLOR})")
      export PS1="${DEFAULT_PS1} hg ${s} \n\\$"
  elif in_svn_repo; then
      local s=$(svn info --show-item revision)
      export PS1="${DEFAULT_PS1} svn (${GREEN}${s}${NO_COLOR})\n\\$"
  else
    export PS1="${DEFAULT_PS1} \n\\$"
  fi
}
